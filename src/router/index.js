import Vue from 'vue';
import Router from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
import AffiliatesList from '@/components/affiliates-list';
import AffiliateDetail from '@/components/affiliate-detail';

Vue.use(BootstrapVue);
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'AffiliatesList',
      component: AffiliatesList,
    },
    {
      path: '/affiliate/:affiliateId/',
      name: 'AffiliateDetail',
      component: AffiliateDetail,
    },
  ],
});
