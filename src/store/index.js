import Vue from 'vue';
import Vuex from 'vuex';
import { mutations } from './mutations';
import * as actions from './actions';

Vue.use(Vuex);

const state = {
  selectedState: '',
  selectedMunicipality: '',
  municipalities: [],
  results: [],
  selectedCategory: '',
  searchTerm: '',
  page: 1,
  affiliateId: '0',
  makeCallToFetchAffiliate: true,
  affiliate: {},
  showMap: false,
  loadingMessage: 'Cargando...',
};

export default new Vuex.Store({
  state,
  mutations,
  actions,
});
