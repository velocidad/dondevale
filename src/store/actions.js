import config from '../../config/index';
import * as types from './mutation-types';
import store from './index';

const resetSearchState = (commit) => {
  commit(types.SET_SEARCH_PAGE, 1);
  commit(types.CLEAR_SEARCH_RESULTS);
};

const resetState = (commit) => {
  commit(types.SET_MUNICIPALITY, '');
  commit(types.SET_MUNICIPALITIES, []);
  resetSearchState(commit);
};

const fetchResults = (commit) => {
  const { selectedState, selectedMunicipality, selectedCategory, searchTerm, page } = store.state;
  const queryString = `?category=${selectedCategory}&state=${selectedState}&municipality=${selectedMunicipality}&search=${searchTerm}&page=${page}`;
  fetch(`${config.apiPath}/api/affiliates/${queryString}`, {
    method: 'GET',
  }).then((response) => {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response.json();
  }).then((json) => {
    commit(types.SET_LOADING_TEXT, 'Cargando...');
    commit(types.SET_SEARCH_RESULTS, json);
  })
    .catch(() => {
      commit(types.SET_LOADING_TEXT, 'Fin de resultados');
      commit(types.SET_SEARCH_RESULTS, { results: [] });
    });
};

export const setLocationState = ({ commit }, value) => {
  resetState(commit);
  commit(types.SET_LOCATION_STATE, value);
  if (value) {
    fetch(`${config.apiPath}/api/location/municipalities/${store.state.selectedState}/`, {
      method: 'GET',
    }).then(response => response.json()).then((json) => {
      commit(types.SET_MUNICIPALITIES, json);
    });
  }
  fetchResults(commit);
};

export const getResults = ({ commit }) => {
  fetchResults(commit);
};

export const setMunicipality = ({ commit }, value) => {
  resetSearchState(commit);
  commit(types.SET_MUNICIPALITY, value);
  fetchResults(commit);
};

export const setCategory = ({ commit }, value) => {
  resetSearchState(commit);
  commit(types.SET_CATEGORY, value);
  fetchResults(commit);
};

export const setSearchTerm = ({ commit }, e) => {
  resetSearchState(commit);
  commit(types.SET_SEARCH_TERM, e.target.value);
  fetchResults(commit);
};

export const increaseSearchPage = ({ commit }) => {
  const { page } = store.state;
  commit(types.SET_SEARCH_PAGE, page + 1);
};

export const handleMapModalVisibility = ({ commit }) => {
  const { showMap } = store.state;
  commit(types.SET_MAP_MODAL_VISIBILITY, !showMap);
};

export const fetchAffiliate = ({ commit }) => {
  const { affiliateId, makeCallToFetchAffiliate } = store.state;
  if (makeCallToFetchAffiliate) {
    fetch(`${config.apiPath}/api/affiliates/${affiliateId}/`, {
      method: 'GET',
    }).then(response => response.json())
      .then((json) => { commit(types.SET_AFFILIATE_OBJECT, json); });
  }
};

export const selectAffiliate = ({ commit }, affiliateId) => {
  const makeCall = affiliateId !== store.state.affiliateId;
  commit(types.SET_MAKE_CALL_FLAG, makeCall);
  commit(types.SET_AFFILIATE_ID, affiliateId);
};
