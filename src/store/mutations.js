import * as types from './mutation-types';

// eslint-disable-next-line import/prefer-default-export
export const mutations = {
  [types.SET_MUNICIPALITIES](state, payload) {
    state.municipalities = [...payload];
  },
  [types.SET_SEARCH_RESULTS](state, payload) {
    state.results.push(...payload.results);
  },
  [types.SET_LOCATION_STATE](state, payload) {
    state.selectedState = payload;
  },
  [types.SET_MUNICIPALITY](state, payload) {
    state.selectedMunicipality = payload;
  },
  [types.SET_CATEGORY](state, payload) {
    state.selectedCategory = payload;
  },
  [types.SET_SEARCH_TERM](state, payload) {
    state.searchTerm = payload;
  },
  [types.SET_SEARCH_PAGE](state, payload) {
    state.page = payload;
  },
  [types.CLEAR_SEARCH_RESULTS](state) {
    state.results = [];
  },
  [types.SET_MAP_MODAL_VISIBILITY](state, isVisible) {
    state.showMap = isVisible;
  },
  [types.SET_AFFILIATE_ID](state, affiliateId) {
    state.affiliateId = affiliateId;
  },
  [types.SET_MAKE_CALL_FLAG](state, makeCall) {
    state.makeCallToFetchAffiliate = makeCall;
  },
  [types.SET_AFFILIATE_OBJECT](state, affiliateObject) {
    state.affiliate = affiliateObject;
  },
  [types.SET_LOADING_TEXT](state, loadingText) {
    state.loadingMessage = loadingText;
  },
};
