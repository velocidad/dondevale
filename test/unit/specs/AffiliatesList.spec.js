import Vue from 'vue';
import sinon from 'sinon';
import AffiliatesList from '../../../src/components/affiliates-list';

import affiliatesList from '../stubs/affiliates-list.stub.json';

describe('AffiliatesList.vue', () => {
  beforeEach(() => {
    sinon.stub(AffiliatesList.methods, 'fetchAssociateData').resolves(affiliatesList);
  });
  afterEach(() => {
    AffiliatesList.methods.fetchAssociateData.restore();
  });
  it('should render correct contents', () => {
    const Constructor = Vue.extend(AffiliatesList);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('.associate').textContent).to.have.length(10);
  });
});
